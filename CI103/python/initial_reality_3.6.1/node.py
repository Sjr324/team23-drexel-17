import pygame
import system


# Class to store the data of a node for the grid in the level up system
class Node:
    def __init__(self, tooltip, row, col):
        self.image = pygame.image.load(system.EMPTY_IMG).convert()
        self.image = pygame.transform.scale(self.image, (system.NODE_SIZE, system.NODE_SIZE))
        self.image_rect = self.image.get_rect()
        self.tooltip = system.MEDIUM_FONT.render(tooltip, 0, system.BLACK)
        self.tooltip_rect = self.tooltip.get_rect()
        self.tooltip_rect.x = system.TOOLTIP_X
        self.tooltip_rect.y = system.TOOLTIP_Y
        self.status = system.EMPTY
        self.row = row
        self.col = col

    def set_image(self, image):
        self.image = pygame.image.load(image).convert()
        self.image = pygame.transform.scale(self.image, (system.NODE_SIZE, system.NODE_SIZE))

    def set_image_x(self, x):
        self.image_rect.x = x

    def set_image_y(self, y):
        self.image_rect.y = y

    def set_tooltip(self, tooltip):
        self.tooltip = system.MEDIUM_FONT.render(tooltip, 0, system.BLACK)

    def set_status(self, status):
        self.status = status

    def get_hover(self):
        if self.image_rect.collidepoint(pygame.mouse.get_pos()):
            return True

    def get_status(self):
        return self.status

    def display_tooltip(self, screen):
        screen.blit(self.tooltip, self.tooltip_rect)

    def display_image(self, screen):
        screen.blit(self.image, self.image_rect)
        if self.status == system.EMPTY:
            self.set_image(system.EMPTY_IMG)
            self.set_tooltip("Unknown")

    # Used to assign the type of cube needed to open a particular node, as well as
    # change the node status when needed
    def manage_node_status(self, cubes, img, tooltip, cube_type):
        if cube_type == system.POWER:
            cube_name = "Power Cube"
        elif cube_type == system.MAGIC:
            cube_name = "Magic Cube"
        elif cube_type == system.MOVEMENT:
            cube_name = "Movement Cube"
        elif cube_type == system.FORTUNE:
            cube_name = "Fortune Cube"
        else:
            cube_name = "Unknown Cube"

        if self.status == system.ACTIVE:
            self.set_image(img)
            self.set_tooltip(tooltip)
        else:
            if self.status == system.PREVIEW:
                self.set_image(system.PREVIEW_IMG)
                self.set_tooltip("Cost: 1 " + cube_name)
                if cubes.get_cube(cube_type) >= 1:
                    self.set_status(system.AVAILABLE)
                    self.set_image(system.AVAILABLE_IMG)
                    self.set_tooltip("Cost: 1 " + cube_name + " (" + tooltip + ")")
            elif self.status == system.AVAILABLE:
                if cubes.get_cube(cube_type) < 1:
                    self.set_status(system.PREVIEW)
                    self.set_image(system.PREVIEW_IMG)

    # Called upon to increase the given stat when a node becomes active
    def increment_stat(self, party, cubes):
        self.status = system.ACTIVE
        if self.row == 0:
            if self.col == 0:
                party[0].add_max_hp(50)
                party[0].add_current_hp(50)
                cubes.remove_cube(system.POWER, 1)
            elif self.col == 1:
                party[0].add_evasion(1)
                cubes.remove_cube(system.MOVEMENT, 1)
            elif self.col == 2:
                party[0].add_strength(1)
                cubes.remove_cube(system.POWER, 1)
            elif self.col == 3:
                party[0].add_defense(1)
                cubes.remove_cube(system.POWER, 1)
            elif self.col == 4:
                party[0].add_magic(2)
                cubes.remove_cube(system.MAGIC, 1)
            elif self.col == 5:
                party[0].add_strength(2)
                cubes.remove_cube(system.POWER, 1)
            elif self.col == 6:
                party[0].add_max_hp(100)
                party[0].add_current_hp(100)
                cubes.remove_cube(system.POWER, 1)
            elif self.col == 7:
                party[0].add_agility(3)
                cubes.remove_cube(system.MOVEMENT, 1)
            elif self.col == 8:
                party[0].add_defense(3)
                cubes.remove_cube(system.POWER, 1)
            elif self.col == 9:
                party[0].add_luck(3)
                cubes.remove_cube(system.FORTUNE, 1)
        elif self.row == 1:
            if self.col == 0:
                party[0].add_magic(1)
                cubes.remove_cube(system.MAGIC, 1)
            elif self.col == 1:
                party[0].add_resistance(1)
                cubes.remove_cube(system.POWER, 1)
            elif self.col == 2:
                party[0].add_max_hp(50)
                party[0].add_current_hp(50)
                cubes.remove_cube(system.POWER, 1)
            elif self.col == 3:
                party[0].add_max_mp(2)
                party[0].add_current_mp(2)
                cubes.remove_cube(system.MAGIC, 1)
            elif self.col == 4:
                party[0].add_accuracy(2)
                cubes.remove_cube(system.MOVEMENT, 1)
            elif self.col == 5:
                party[0].add_defense(2)
                cubes.remove_cube(system.POWER, 1)
            elif self.col == 6:
                party[0].add_evasion(3)
                cubes.remove_cube(system.MOVEMENT, 1)
            elif self.col == 7:
                party[0].add_resistance(3)
                cubes.remove_cube(system.POWER, 1)
            elif self.col == 8:
                party[0].add_strength(3)
                cubes.remove_cube(system.POWER, 1)
            elif self.col == 9:
                party[0].add_accuracy(4)
                cubes.remove_cube(system.MOVEMENT, 1)
        elif self.row == 2:
            if self.col == 0:
                party[0].add_max_mp(1)
                party[0].add_current_mp(1)
                cubes.remove_cube(system.MAGIC, 1)
            elif self.col == 1:
                party[0].add_agility(1)
                cubes.remove_cube(system.MOVEMENT, 1)
            elif self.col == 2:
                party[0].add_magic(2)
                cubes.remove_cube(system.MAGIC, 1)
            elif self.col == 3:
                party[0].add_strength(2)
                cubes.remove_cube(system.POWER, 1)
            elif self.col == 4:
                party[0].add_resistance(2)
                cubes.remove_cube(system.MAGIC, 1)
            elif self.col == 5:
                party[0].add_evasion(3)
                cubes.remove_cube(system.MOVEMENT, 1)
            elif self.col == 6:
                party[0].add_max_hp(150)
                party[0].add_current_hp(150)
                cubes.remove_cube(system.POWER, 1)
            elif self.col == 7:
                party[0].add_agility(3)
                cubes.remove_cube(system.MOVEMENT, 1)
            elif self.col == 8:
                party[0].add_defense(4)
                cubes.remove_cube(system.POWER, 1)
            elif self.col == 9:
                party[0].add_strength(4)
                cubes.remove_cube(system.POWER, 1)
        elif self.row == 3:
            if self.col == 0:
                party[0].add_accuracy(1)
                cubes.remove_cube(system.MOVEMENT, 1)
            elif self.col == 1:
                party[0].add_resistance(2)
                cubes.remove_cube(system.MAGIC, 1)
            elif self.col == 2:
                party[0].add_defense(2)
                cubes.remove_cube(system.POWER, 1)
            elif self.col == 3:
                party[0].add_evasion(2)
                cubes.remove_cube(system.MOVEMENT, 1)
            elif self.col == 4:
                party[0].add_max_hp(150)
                party[0].add_current_hp(150)
                cubes.remove_cube(system.POWER, 1)
            elif self.col == 5:
                party[0].add_magic(3)
                cubes.remove_cube(system.MAGIC, 1)
            elif self.col == 6:
                party[0].add_agility(3)
                cubes.remove_cube(system.MOVEMENT, 1)
            elif self.col == 7:
                party[0].add_max_mp(4)
                party[0].add_current_mp(4)
                cubes.remove_cube(system.MAGIC, 1)
            elif self.col == 8:
                party[0].add_accuracy(4)
                cubes.remove_cube(system.MOVEMENT, 1)
            elif self.col == 9:
                party[0].add_max_hp(250)
                party[0].add_current_hp(250)
                cubes.remove_cube(system.POWER, 1)
        elif self.row == 4:
            if self.col == 0:
                party[0].add_luck(2)
                cubes.remove_cube(system.FORTUNE, 1)
            elif self.col == 1:
                party[0].add_magic(2)
                cubes.remove_cube(system.MAGIC, 1)
            elif self.col == 2:
                party[0].add_max_mp(2)
                party[0].add_current_mp(2)
                cubes.remove_cube(system.MAGIC, 1)
            elif self.col == 3:
                party[0].add_agility(3)
                cubes.remove_cube(system.MOVEMENT, 1)
            elif self.col == 4:
                party[0].add_defense(3)
                cubes.remove_cube(system.POWER, 1)
            elif self.col == 5:
                party[0].add_resistance(3)
                cubes.remove_cube(system.MAGIC, 1)
            elif self.col == 6:
                party[0].add_max_mp(4)
                party[0].add_current_mp(4)
                cubes.remove_cube(system.MAGIC, 1)
            elif self.col == 7:
                party[0].add_accuracy(4)
                cubes.remove_cube(system.MOVEMENT, 1)
            elif self.col == 8:
                party[0].add_magic(5)
                cubes.remove_cube(system.MAGIC, 1)
            elif self.col == 9:
                party[0].add_luck(7)
                cubes.remove_cube(system.FORTUNE, 1)
