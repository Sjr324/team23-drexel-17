import system
import button


# Class to store all inventory items
# Items are shared between party members
class Items:
    def __init__(self):
        self.potions = 0
        self.elixirs = 0
        self.revives = 0

    def set_potions(self, potions):
        self.potions = potions

    def set_elixirs(self, elixirs):
        self.elixirs = elixirs

    def set_revives(self, revives):
        self.revives = revives

    def add_potions(self, potions):
        self.potions += potions

    def add_elixirs(self, elixirs):
        self.elixirs += elixirs

    def add_revives(self, revives):
        self.revives += revives

    def get_potions(self):
        return self.potions

    def get_elixirs(self):
        return self.elixirs

    def get_revives(self):
        return self.revives

    # Displays all items to screen
    def display(self, screen, event, party, x, y):
        potions_button = button.Button("POTIONS: " + str(self.potions), "Restores 100 HP", system.WHITE)
        elixirs_button = button.Button("ELIXIRS: " + str(self.elixirs), "Restores 10 MP", system.WHITE)
        revives_button = button.Button("REVIVES: " + str(self.revives), "Reanimates character", system.WHITE)
        potions_button.display_left_aligned(screen, x, y)
        elixirs_button.display_left_aligned(screen, x, y + system.ITEM_SPACING_Y)
        revives_button.display_left_aligned(screen, x, y + (2 * system.ITEM_SPACING_Y))
        if potions_button.get_clicked(event) and self.potions > 0 and party[0].get_current_hp() != party[0].get_max_hp():
            party[0].add_current_hp(100)
            self.potions -= 1
        elif elixirs_button.get_clicked(event) and self.elixirs > 0 and party[0].get_current_mp() != party[0].get_max_mp():
            party[0].add_current_mp(10)
            self.elixirs -= 1


# Class to store all cubes in inventory
# NOTE: Cubes are items that can be redeemed for stat upgrades, or traded for items
# The party as a whole shares cubes, they are not player dependant
class Cubes:
    def __init__(self):
        self.power = 0
        self.magic = 0
        self.movement = 0
        self.fortune = 0

    def set_cube(self, cube, value):
        if cube == system.POWER:
            self.power = value
        elif cube == system.MAGIC:
            self.magic = value
        elif cube == system.MOVEMENT:
            self.movement = value
        elif cube == system.FORTUNE:
            self.fortune = value

    def add_cube(self, cube, value):
        if cube == system.POWER:
            self.power += value
        elif cube == system.MAGIC:
            self.magic += value
        elif cube == system.MOVEMENT:
            self.movement += value
        elif cube == system.FORTUNE:
            self.fortune += value

    def remove_cube(self, cube, value):
        if cube == system.POWER:
            self.power -= value
        elif cube == system.MAGIC:
            self.magic -= value
        elif cube == system.MOVEMENT:
            self.movement -= value
        elif cube == system.FORTUNE:
            self.fortune -= value

    def get_cube(self, cube):
        if cube == system.POWER:
            return self.power
        elif cube == system.MAGIC:
            return self.magic
        elif cube == system.MOVEMENT:
            return self.movement
        elif cube == system.FORTUNE:
            return self.fortune
