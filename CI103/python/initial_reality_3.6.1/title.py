import pygame
import system
import button


# Class used to display the title screen
class Title:
    def __init__(self):
        self.battle_button = button.Button("Battle", "", system.BLACK)
        self.grid_button = button.Button("Level Up", "", system.BLACK)
        self.shop_button = button.Button("Shop", "", system.BLACK)
        self.save_button = button.Button("SAVE", "", system.BLACK)
        self.load_button = button.Button("LOAD", "", system.BLACK)
        self.logo = pygame.image.load("./resources/images/title/logo.png")
        self.logo = pygame.transform.scale(self.logo, (856, 168))
        self.logo_rect = self.logo.get_rect()
        self.logo_rect.center = (system.WINDOW_CENTER_X, system.WINDOW_CENTER_Y - 200)
        self.background = pygame.image.load("./resources/images/title/background.png")

    def run(self, screen, event, current_screen):
        pygame.display.set_caption("Initial Reality")
        screen.blit(self.background, (0, 0))
        screen.blit(self.logo, self.logo_rect)

        self.battle_button.display_centered(screen, system.WINDOW_CENTER_Y)
        self.grid_button.display_centered(screen, system.WINDOW_CENTER_Y + system.BUTTON_SPACING)
        self.shop_button.display_centered(screen, system.WINDOW_CENTER_Y + (2 * system.BUTTON_SPACING))

        if self.battle_button.get_clicked(event):
            current_screen.launch_battle()
        elif self.grid_button.get_clicked(event):
            current_screen.launch_grid()
        elif self.shop_button.get_clicked(event):
            current_screen.launch_shop()
