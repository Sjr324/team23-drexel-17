import pygame
import system
import node


# Class that stores the grid of nodes used on the level up screen
class Grid:
    def __init__(self):
        # Creates a list of nodes
        self.nodes = [[0 for x in range(system.COLUMNS)] for x in range(system.ROWS)]
        # Cycles through nodes list and assigns blank tooltips, as well as X and Y positions
        for row in range(len(self.nodes)):
            for column in range(len(self.nodes[0])):
                self.nodes[row][column] = node.Node("", row, column)
                self.nodes[row][column].set_image_x(system.GRID_X + (column * system.NODE_SIZE))
                self.nodes[row][column].set_image_y(system.GRID_Y + (row * system.NODE_SIZE))
        # Initializes first node X and Y, image, and status, which all following nodes will use for positioning
        self.nodes[0][0].set_image_x(system.GRID_X)
        self.nodes[0][0].set_image_y(system.GRID_Y)
        self.nodes[0][0].set_status(system.PREVIEW)
        self.nodes[0][0].set_image(system.PREVIEW_IMG)
        # Sets current character to the first party member
        self.current_character = 1

    # Method manages what happens when the mouse clicks on a node
    def node_click(self, event, party, cubes):
        # Cycles through all nodes
        for row in range(len(self.nodes)):
            for column in range(len(self.nodes[0])):
                # If the node is hovered on and clicked on
                if self.nodes[row][column].get_hover():
                    if event.type == pygame.MOUSEBUTTONDOWN:

                        # Checks if the node is available
                        # NOTE: Nodes have 4 statuses: EMPTY, PREVIEW, AVAILABLE, and ACTIVE
                        # EMPTY - The node is gray, no tooltip
                        # PREVIEW - The node is blue, tooltip displaying cost
                        # AVAILABLE - The node is green, tooltip displaying stat upgrade and cost
                        # ACTIVE - The node is gold, tooltip dislays the stat upgrade received
                        if self.nodes[row][column].get_status() == system.AVAILABLE:
                            # Increments the stat the node is assigned, changes the statuses of all 4 surrounding nodes
                            self.nodes[row][column].increment_stat(party, cubes)
                            if row + 1 < system.ROWS and self.nodes[row + 1][column].get_status() == system.EMPTY:
                                self.nodes[row + 1][column].set_status(system.PREVIEW)
                            if row - 1 >= 0 and self.nodes[row - 1][column].get_status() == system.EMPTY:
                                self.nodes[row - 1][column].set_status(system.PREVIEW)
                            if column + 1 < system.COLUMNS and self.nodes[row][
                                        column + 1].get_status() == system.EMPTY:
                                self.nodes[row][column + 1].set_status(system.PREVIEW)
                            if column - 1 >= 0 and self.nodes[row][column - 1].get_status() == system.EMPTY:
                                self.nodes[row][column - 1].set_status(system.PREVIEW)

    # Displays the stats of the selected character, as well as the cubes of the party
    def display_stats(self, screen, event, party, cubes):
        choose_character_text = system.MEDIUM_FONT.render(str(party[0].get_name()), 0, system.RED)
        choose_character_text_rect = choose_character_text.get_rect()
        choose_character_text_rect.x = system.TOOLTIP_X
        choose_character_text_rect.y = system.GRID_BOTTOM + system.STAT_GRID_OFFSET_Y

        hp_stat_text = system.MEDIUM_FONT.render("Health:     " + str(party[0].get_max_hp()), 0, system.BLACK)
        hp_stat_text_rect = hp_stat_text.get_rect()
        hp_stat_text_rect.x = choose_character_text_rect.x
        hp_stat_text_rect.y = choose_character_text_rect.y + system.GRID_STAT_SPACING_Y

        strength_stat_text = system.MEDIUM_FONT.render("Strength:   " + str(party[0].get_strength()), 0,
                                                       system.BLACK)
        strength_stat_text_rect = strength_stat_text.get_rect()
        strength_stat_text_rect.x = choose_character_text_rect.x
        strength_stat_text_rect.y = hp_stat_text_rect.y + system.GRID_STAT_SPACING_Y

        defense_stat_text = system.MEDIUM_FONT.render("Defense:    " + str(party[0].get_defense()), 0, system.BLACK)
        defense_stat_text_rect = defense_stat_text.get_rect()
        defense_stat_text_rect.x = choose_character_text_rect.x
        defense_stat_text_rect.y = strength_stat_text_rect.y + system.GRID_STAT_SPACING_Y

        magic_stat_text = system.MEDIUM_FONT.render("Magic:      " + str(party[0].get_magic()), 0, system.BLACK)
        magic_stat_text_rect = magic_stat_text.get_rect()
        magic_stat_text_rect.x = choose_character_text_rect.x
        magic_stat_text_rect.y = defense_stat_text_rect.y + system.GRID_STAT_SPACING_Y

        resistance_stat_text = system.MEDIUM_FONT.render("Resistance: " + str(party[0].get_resistance()), 0,
                                                         system.BLACK)
        resistance_stat_text_rect = resistance_stat_text.get_rect()
        resistance_stat_text_rect.x = choose_character_text_rect.x
        resistance_stat_text_rect.y = magic_stat_text_rect.y + system.GRID_STAT_SPACING_Y

        mp_stat_text = system.MEDIUM_FONT.render("Mana:     " + str(party[0].get_max_mp()), 0, system.BLACK)
        mp_stat_text_rect = mp_stat_text.get_rect()
        mp_stat_text_rect.x = choose_character_text_rect.x + system.GRID_STAT_SPACING_X
        mp_stat_text_rect.y = choose_character_text_rect.y + system.GRID_STAT_SPACING_Y

        agility_stat_text = system.MEDIUM_FONT.render("Agility:  " + str(party[0].get_agility()), 0, system.BLACK)
        agility_stat_text_rect = agility_stat_text.get_rect()
        agility_stat_text_rect.x = mp_stat_text_rect.x
        agility_stat_text_rect.y = mp_stat_text_rect.y + system.GRID_STAT_SPACING_Y

        evasion_stat_text = system.MEDIUM_FONT.render("Evasion:  " + str(party[0].get_evasion()), 0, system.BLACK)
        evasion_stat_text_rect = evasion_stat_text.get_rect()
        evasion_stat_text_rect.x = mp_stat_text_rect.x
        evasion_stat_text_rect.y = agility_stat_text_rect.y + system.GRID_STAT_SPACING_Y

        accuracy_stat_text = system.MEDIUM_FONT.render("Accuracy: " + str(party[0].get_accuracy()), 0,
                                                       system.BLACK)
        accuracy_stat_text_rect = accuracy_stat_text.get_rect()
        accuracy_stat_text_rect.x = mp_stat_text_rect.x
        accuracy_stat_text_rect.y = evasion_stat_text_rect.y + system.GRID_STAT_SPACING_Y

        luck_stat_text = system.MEDIUM_FONT.render("Luck:     " + str(party[0].get_luck()), 0, system.BLACK)
        luck_stat_text_rect = luck_stat_text.get_rect()
        luck_stat_text_rect.x = mp_stat_text_rect.x
        luck_stat_text_rect.y = accuracy_stat_text_rect.y + system.GRID_STAT_SPACING_Y

        cubes_text = system.MEDIUM_FONT.render("-- Cubes --", 0, system.BLACK)
        cubes_text_rect = cubes_text.get_rect()
        cubes_text_rect.x = mp_stat_text_rect.x + system.GRID_STAT_SPACING_X_2
        cubes_text_rect.y = mp_stat_text_rect.y

        power_cubes_text = system.MEDIUM_FONT.render("Power:    " + str(cubes.get_cube(system.POWER)), 0, system.BLACK)
        power_cubes_text_rect = power_cubes_text.get_rect()
        power_cubes_text_rect.x = mp_stat_text_rect.x + system.GRID_STAT_SPACING_X_2
        power_cubes_text_rect.y = cubes_text_rect.y + system.GRID_STAT_SPACING_Y

        magic_cubes_text = system.MEDIUM_FONT.render("Magic:    " + str(cubes.get_cube(system.MAGIC)), 0, system.BLACK)
        magic_cubes_text_rect = magic_cubes_text.get_rect()
        magic_cubes_text_rect.x = power_cubes_text_rect.x
        magic_cubes_text_rect.y = power_cubes_text_rect.y + system.GRID_STAT_SPACING_Y

        movement_cubes_text = system.MEDIUM_FONT.render("Movement: " + str(cubes.get_cube(system.MOVEMENT)), 0, system.BLACK)
        movement_cubes_text_rect = movement_cubes_text.get_rect()
        movement_cubes_text_rect.x = power_cubes_text_rect.x
        movement_cubes_text_rect.y = magic_cubes_text_rect.y + system.GRID_STAT_SPACING_Y

        fortune_cubes_text = system.MEDIUM_FONT.render("Fortune:  " + str(cubes.get_cube(system.FORTUNE)), 0, system.BLACK)
        fortune_cubes_text_rect = fortune_cubes_text.get_rect()
        fortune_cubes_text_rect.x = power_cubes_text_rect.x
        fortune_cubes_text_rect.y = movement_cubes_text_rect.y + system.GRID_STAT_SPACING_Y

        # Displays tooltips for all available text in the menu
        # If the character name is clicked on, the current character is swapped
        if choose_character_text_rect.collidepoint(pygame.mouse.get_pos()):
            tooltip_text = "Click to change current character"
            if event.type == pygame.MOUSEBUTTONDOWN:
                if self.current_character >= len(party) - 1:
                    self.current_character = 1
                else:
                    self.current_character += 1
                party[0] = party[self.current_character]
        elif hp_stat_text_rect.collidepoint(pygame.mouse.get_pos()):
            tooltip_text = "Determines how many hits can be taken"
        elif mp_stat_text_rect.collidepoint(pygame.mouse.get_pos()):
            tooltip_text = "Determines how many spells can be cast"
        elif strength_stat_text_rect.collidepoint(pygame.mouse.get_pos()):
            tooltip_text = "Determines attack damage dealt"
        elif defense_stat_text_rect.collidepoint(pygame.mouse.get_pos()):
            tooltip_text = "Determines attack damage received"
        elif magic_stat_text_rect.collidepoint(pygame.mouse.get_pos()):
            tooltip_text = "Determines magic damage dealt"
        elif resistance_stat_text_rect.collidepoint(pygame.mouse.get_pos()):
            tooltip_text = "Determines magic damage received"
        elif agility_stat_text_rect.collidepoint(pygame.mouse.get_pos()):
            tooltip_text = "Determines turn order"
        elif evasion_stat_text_rect.collidepoint(pygame.mouse.get_pos()):
            tooltip_text = "Determines dodging probability"
        elif accuracy_stat_text_rect.collidepoint(pygame.mouse.get_pos()):
            tooltip_text = "Determines miss rate"
        elif luck_stat_text_rect.collidepoint(pygame.mouse.get_pos()):
            tooltip_text = "Determines critical hit chance"
        elif cubes_text_rect.collidepoint(pygame.mouse.get_pos()):
            tooltip_text = "Cubes allow you to unlock certain nodes"
        elif power_cubes_text_rect.collidepoint(pygame.mouse.get_pos()):
            tooltip_text = "Can unlock Health, Strength, or Defense"
        elif magic_cubes_text_rect.collidepoint(pygame.mouse.get_pos()):
            tooltip_text = "Can unlock Mana, Magic, or Resistance"
        elif movement_cubes_text_rect.collidepoint(pygame.mouse.get_pos()):
            tooltip_text = "Can unlock Speed, Evasion, or Accuracy"
        elif fortune_cubes_text_rect.collidepoint(pygame.mouse.get_pos()):
            tooltip_text = "Can unlock Luck"
        else:
            tooltip_text = ""

        tooltip = system.MEDIUM_FONT.render(tooltip_text, 0, system.BLACK)
        tooltip_rect = tooltip.get_rect()
        tooltip_rect.x = system.TOOLTIP_X
        tooltip_rect.y = system.TOOLTIP_Y

        # Displays everything to the screen
        screen.blit(choose_character_text, choose_character_text_rect)
        screen.blit(hp_stat_text, hp_stat_text_rect)
        screen.blit(mp_stat_text, mp_stat_text_rect)
        screen.blit(strength_stat_text, strength_stat_text_rect)
        screen.blit(defense_stat_text, defense_stat_text_rect)
        screen.blit(magic_stat_text, magic_stat_text_rect)
        screen.blit(resistance_stat_text, resistance_stat_text_rect)
        screen.blit(agility_stat_text, agility_stat_text_rect)
        screen.blit(evasion_stat_text, evasion_stat_text_rect)
        screen.blit(accuracy_stat_text, accuracy_stat_text_rect)
        screen.blit(luck_stat_text, luck_stat_text_rect)
        screen.blit(cubes_text, cubes_text_rect)
        screen.blit(power_cubes_text, power_cubes_text_rect)
        screen.blit(magic_cubes_text, magic_cubes_text_rect)
        screen.blit(movement_cubes_text, movement_cubes_text_rect)
        screen.blit(fortune_cubes_text, fortune_cubes_text_rect)
        screen.blit(tooltip, tooltip_rect)

    # Method to display all data
    def display(self, screen, event, party, cubes):
        self.display_stats(screen, event, party, cubes)
        self.node_click(event, party, cubes)
        for row in range(len(self.nodes)):
            for column in range(len(self.nodes[0])):
                self.nodes[row][column].display_image(screen)
                if self.nodes[row][column].get_hover():
                    self.nodes[row][column].display_tooltip(screen)
        # Assigns status to each individual node
        self.nodes[0][0].manage_node_status(cubes, system.HP_IMG, "+50 HP", system.POWER)
        self.nodes[0][1].manage_node_status(cubes, system.EVASION_IMG, "+1 Evasion", system.MOVEMENT)
        self.nodes[0][2].manage_node_status(cubes, system.STRENGTH_IMG, "+1 Strength", system.POWER)
        self.nodes[0][3].manage_node_status(cubes, system.DEFENSE_IMG, "+1 Defense", system.POWER)
        self.nodes[0][4].manage_node_status(cubes, system.MAGIC_IMG, "+2 Magic", system.MAGIC)
        self.nodes[0][5].manage_node_status(cubes, system.STRENGTH_IMG, "+2 Strength", system.POWER)
        self.nodes[0][6].manage_node_status(cubes, system.HP_IMG, "+100 HP", system.POWER)
        self.nodes[0][7].manage_node_status(cubes, system.AGILITY_IMG, "+3 Agility", system.MOVEMENT)
        self.nodes[0][8].manage_node_status(cubes, system.DEFENSE_IMG, "+3 Defense", system.POWER)
        self.nodes[0][9].manage_node_status(cubes, system.LUCK_IMG, "+3 Luck", system.FORTUNE)

        self.nodes[1][0].manage_node_status(cubes, system.MAGIC_IMG, "+1 Magic", system.MAGIC)
        self.nodes[1][1].manage_node_status(cubes, system.RESISTANCE_IMG, "+1 Resistance", system.MAGIC)
        self.nodes[1][2].manage_node_status(cubes, system.HP_IMG, "+50 HP", system.POWER)
        self.nodes[1][3].manage_node_status(cubes, system.MP_IMG, "+2 MP", system.MAGIC)
        self.nodes[1][4].manage_node_status(cubes, system.ACCURACY_IMG, "+2 Accuracy", system.MOVEMENT)
        self.nodes[1][5].manage_node_status(cubes, system.DEFENSE_IMG, "+2 Defense", system.POWER)
        self.nodes[1][6].manage_node_status(cubes, system.EVASION_IMG, "+3 Evasion", system.MOVEMENT)
        self.nodes[1][7].manage_node_status(cubes, system.RESISTANCE_IMG, "+3 Resistance", system.MAGIC)
        self.nodes[1][8].manage_node_status(cubes, system.STRENGTH_IMG, "+3 Strength", system.POWER)
        self.nodes[1][9].manage_node_status(cubes, system.ACCURACY_IMG, "+4 Accuracy", system.MOVEMENT)

        self.nodes[2][0].manage_node_status(cubes, system.MP_IMG, "+1 MP", system.MAGIC)
        self.nodes[2][1].manage_node_status(cubes, system.AGILITY_IMG, "+1 Agility", system.MOVEMENT)
        self.nodes[2][2].manage_node_status(cubes, system.MAGIC_IMG, "+2 Magic", system.MAGIC)
        self.nodes[2][3].manage_node_status(cubes, system.STRENGTH_IMG, "+2 Strength", system.POWER)
        self.nodes[2][4].manage_node_status(cubes, system.RESISTANCE_IMG, "+2 Resistance", system.MAGIC)
        self.nodes[2][5].manage_node_status(cubes, system.EVASION_IMG, "+3 Evasion", system.MOVEMENT)
        self.nodes[2][6].manage_node_status(cubes, system.HP_IMG, "+150 HP", system.POWER)
        self.nodes[2][7].manage_node_status(cubes, system.AGILITY_IMG, "+3 Agility", system.MOVEMENT)
        self.nodes[2][8].manage_node_status(cubes, system.DEFENSE_IMG, "+4 Defense", system.POWER)
        self.nodes[2][9].manage_node_status(cubes, system.STRENGTH_IMG, "+4 Strength", system.POWER)

        self.nodes[3][0].manage_node_status(cubes, system.ACCURACY_IMG, "+1 Accuracy", system.MOVEMENT)
        self.nodes[3][1].manage_node_status(cubes, system.RESISTANCE_IMG, "+2 Resistance", system.MAGIC)
        self.nodes[3][2].manage_node_status(cubes, system.DEFENSE_IMG, "+2 Defense", system.POWER)
        self.nodes[3][3].manage_node_status(cubes, system.EVASION_IMG, "+2 Evasion", system.MOVEMENT)
        self.nodes[3][4].manage_node_status(cubes, system.HP_IMG, "+150 HP", system.POWER)
        self.nodes[3][5].manage_node_status(cubes, system.MAGIC_IMG, "+3 Magic", system.MAGIC)
        self.nodes[3][6].manage_node_status(cubes, system.AGILITY_IMG, "+3 Agility", system.MOVEMENT)
        self.nodes[3][7].manage_node_status(cubes, system.MP_IMG, "+4 MP", system.MAGIC)
        self.nodes[3][8].manage_node_status(cubes, system.ACCURACY_IMG, "+4 Accuracy", system.MOVEMENT)
        self.nodes[3][9].manage_node_status(cubes, system.HP_IMG, "+250 HP", system.POWER)

        self.nodes[4][0].manage_node_status(cubes, system.LUCK_IMG, "+2 Luck", system.FORTUNE)
        self.nodes[4][1].manage_node_status(cubes, system.MAGIC_IMG, "+2 Magic", system.MAGIC)
        self.nodes[4][2].manage_node_status(cubes, system.MP_IMG, "+2 MP", system.MAGIC)
        self.nodes[4][3].manage_node_status(cubes, system.AGILITY_IMG, "+3 Agility", system.MOVEMENT)
        self.nodes[4][4].manage_node_status(cubes, system.DEFENSE_IMG, "+3 Defense", system.POWER)
        self.nodes[4][5].manage_node_status(cubes, system.RESISTANCE_IMG, "+3 Resistance", system.MAGIC)
        self.nodes[4][6].manage_node_status(cubes, system.MP_IMG, "+4 MP", system.MAGIC)
        self.nodes[4][7].manage_node_status(cubes, system.ACCURACY_IMG, "+4 Accuracy", system.MOVEMENT)
        self.nodes[4][8].manage_node_status(cubes, system.MAGIC_IMG, "+5 Magic", system.MAGIC)
        self.nodes[4][9].manage_node_status(cubes, system.LUCK_IMG, "+7 Luck", system.FORTUNE)
