import pygame
import system
import button


# Class sued to create and store character and enemy sprites, names, and stats
class Character:
    def __init__(self, name, max_hp, max_mp, strength, defense, magic, resistance, agility, evasion, accuracy, luck,
                 idle_sprite, attack_sprite):
        self.name = name
        self.max_hp = max_hp
        self.max_mp = max_mp
        self.current_hp = max_hp
        self.current_mp = max_mp
        self.strength = strength
        self.defense = defense
        self.magic = magic
        self.resistance = resistance
        self.agility = agility
        self.evasion = evasion
        self.accuracy = accuracy
        self.luck = luck
        self.spells = []
        self.large_font = pygame.font.Font("./resources/fonts/pixel.ttf", 40)
        self.small_font = pygame.font.Font("./resources/fonts/pixel.ttf", 18)
        self.idle_sprite = pygame.image.load(idle_sprite)
        self.idle_sprite = pygame.transform.scale(self.idle_sprite, (system.SPRITE_WIDTH, system.SPRITE_HEIGHT))
        self.attack_sprite = pygame.image.load(attack_sprite)
        self.attack_sprite = pygame.transform.scale(self.attack_sprite, (system.SPRITE_WIDTH, system.SPRITE_HEIGHT))
        self.rect = self.idle_sprite.get_rect()

        #rishi's code
        self.is_dead = False

    def set_name(self, name):
        self.name = name

    def set_max_hp(self, max_hp):
        self.max_hp = max_hp

    def set_max_mp(self, max_mp):
        self.max_mp = max_mp

    def set_current_hp(self, current_hp):
        self.current_hp = current_hp

    def set_current_mp(self, current_mp):
        self.current_mp = current_mp

    def set_strength(self, strength):
        self.strength = strength

    def set_defense(self, defense):
        self.defense = defense

    def set_magic(self, magic):
        self.magic = magic

    def set_resistance(self, resistance):
        self.resistance = resistance

    def set_agility(self, agility):
        self.agility = agility

    def set_evasion(self, evasion):
        self.evasion = evasion

    def set_accuracy(self, accuracy):
        self.accuracy = accuracy

    def set_luck(self, luck):
        self.luck = luck

    def add_max_hp(self, max_hp):
        self.max_hp += max_hp

    def add_max_mp(self, max_mp):
        self.max_mp += max_mp

    def add_current_hp(self, current_hp):
        self.current_hp += current_hp
        if self.current_hp > self.max_hp:
            self.current_hp = self.max_hp

    def add_current_mp(self, current_mp):
        self.current_mp += current_mp
        if self.current_mp > self.max_mp:
            self.current_mp = self.max_mp

    def add_strength(self, strength):
        self.strength += strength

    def add_defense(self, defense):
        self.defense += defense

    def add_magic(self, magic):
        self.magic += magic

    def add_resistance(self, resistance):
        self.resistance += resistance

    def add_agility(self, agility):
        self.agility += agility

    def add_evasion(self, evasion):
        self.evasion += evasion

    def add_accuracy(self, accuracy):
        self.accuracy += accuracy

    def add_luck(self, luck):
        self.luck += luck

    def remove_current_hp(self, hp):
        self.current_hp -= hp
        if self.current_hp < 0:
            self.current_hp = 0

    def remove_current_mp(self, mp):
        self.current_mp -= mp
        if self.current_mp < 0:
            self.current_mp = 0

    def get_name(self):
        return self.name

    def get_max_hp(self):
        return self.max_hp

    def get_max_mp(self):
        return self.max_mp

    def get_current_hp(self):
        return self.current_hp

    def get_current_mp(self):
        return self.current_mp

    def get_strength(self):
        return self.strength

    def get_defense(self):
        return self.defense

    def get_magic(self):
        return self.magic

    def get_resistance(self):
        return self.resistance

    def get_agility(self):
        return self.agility

    def get_evasion(self):
        return self.evasion

    def get_accuracy(self):
        return self.accuracy

    def get_luck(self):
        return self.luck

    # Adds a spell to the character's spell list
    def add_spell(self, text, tooltip):
        spell = button.Button(text, tooltip, system.WHITE)
        self.spells.append(spell)

    def get_spell(self, n):
        return self.spells[n]

    # Renders spells to screen
    def display_spells(self, screen):
        spell_x = system.SPELL_INITIAL_X
        spell_y = system.SPELL_INITIAL_Y
        for spell in range(len(self.spells)):
            self.spells[spell].display_left_aligned(screen, spell_x, spell_y)
            spell_y += system.SPELL_SPACING_Y
            if spell_y >= system.SPELL_MAX_Y:
                spell_y = system.SPELL_INITIAL_Y
                spell_x += system.SPELL_SPACING_X

    # Renders stats to screen
    def display_stats(self, screen, x, y):
        name_text = self.large_font.render(self.name, 0, system.WHITE)
        name_text_rect = name_text.get_rect()
        health_text = self.small_font.render("HP:" + str(self.max_hp) + " / " + str(self.current_hp), 0, system.WHITE)
        health_text_rect = health_text.get_rect()
        mana_text = self.small_font.render("MP:" + str(self.max_mp) + " / " + str(self.current_mp), 0, system.WHITE)
        mana_text_rect = mana_text.get_rect()
        
        name_text_rect.right = x
        name_text_rect.y = y

        health_text_rect.x = x + system.STAT_SPACING_X
        health_text_rect.y = name_text_rect.y

        mana_text_rect.x = health_text_rect.x
        mana_text_rect.y = health_text_rect.y + system.STAT_SPACING_Y

        screen.blit(name_text, name_text_rect)
        screen.blit(health_text, health_text_rect)
        screen.blit(mana_text, mana_text_rect)

    # This sprite is used when no attack is in progress
    def display_idle_sprite(self, screen, x, y):
        self.rect.x = x
        self.rect.y = y
        screen.blit(self.idle_sprite, self.rect)

    # This sprite is rendered when an attack is used
    def display_attack_sprite(self, screen, x, y):
        self.rect.x = x
        self.rect.y = y
        screen.blit(self.attack_sprite, self.rect)

    # Returns true if mouse hovers over the sprite
    def get_hover(self):
        if self.rect.collidepoint(pygame.mouse.get_pos()):
            return True

    # Returns true if the sprite is clicked on
    def get_clicked(self, event):
        if self.rect.collidepoint(pygame.mouse.get_pos()):
            if event.type == pygame.MOUSEBUTTONDOWN:
                return True

    #rishi's code
    def kill(self):
        if(self.current_hp == 0):
            self.idle_sprite = pygame.image.load(system.DEATH_SPRITE)
            self.idle_sprite = pygame.transform.scale(self.idle_sprite, (75, 150))
            self.attack_sprite = pygame.image.load(system.DEATH_SPRITE)
            self.attack_sprite = pygame.transform.scale(self.attack_sprite, (75, 150))
            self.set_strength(0)
            self.set_magic(0)
            self.is_dead = True

    def has_died(self):
        return self.is_dead