import pygame
import system
import grid


# Class for the level up screen
class Level:
    def __init__(self):
        self.background = pygame.image.load("./resources/images/grid/background.png").convert()
        self.info_box = pygame.image.load("./resources/images/grid/info_box.png").convert()
        self.stat_box = pygame.image.load("./resources/images/grid/stat_box.png").convert()
        self.grid = grid.Grid()

    def display(self, screen, event, party, cubes):
        pygame.display.set_caption("Level Up")
        screen.blit(self.background, (0, 0))
        screen.blit(self.info_box, (system.GRID_X, 0))
        screen.blit(self.stat_box, (system.GRID_X, system.GRID_BOTTOM))
        self.grid.display(screen, event, party, cubes)
