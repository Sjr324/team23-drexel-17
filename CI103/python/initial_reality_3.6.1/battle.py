import pygame
import system
import button
import random

class Battle:
    def __init__(self):
        super().__init__()
        self.background = pygame.image.load("./resources/images/battle/background.png")
        self.info_box = pygame.image.load("./resources/images/battle/info_box.png")
        self.battle_box = pygame.image.load("./resources/images/battle/battle_box.png")

        self.attack_button = button.Button("ATTACK", "Attack an opponent", system.WHITE)
        self.magic_button = button.Button("MAGIC", "Cast a spell on an opponent", system.WHITE)
        self.item_button = button.Button("ITEM", "Use an item on yourself", system.WHITE)

        self.characters = []
        self.enemies = []

        self.attack_active = False
        self.spells_active = False
        self.items_active = False

        self.turn = 1
        self.battle_over = False
        self.tooltip = ""


    def display_menu(self, screen, event, party, items, enemies):
        pygame.display.set_caption('Battle')
        screen.blit(self.background, (0, 0))
        screen.blit(self.info_box, (system.GRID_X, 0))
        screen.blit(self.battle_box, (0, system.WINDOW_HEIGHT - 200))

        self.attack_button.display_left_aligned(screen, system.BUTTON_X, system.BUTTON_Y)
        self.magic_button.display_left_aligned(screen, system.BUTTON_X, (system.BUTTON_Y + system.BUTTON_SPACING_Y))
        self.item_button.display_left_aligned(screen, system.BUTTON_X, (system.BUTTON_Y + (2 * system.BUTTON_SPACING_Y)))

        for member in range(1, len(party)):
            party[member].display_stats(screen, system.NAME_X, system.NAME_Y + ((member - 1) * system.NAME_SPACING_Y))

        if self.check_game(party, enemies):
            True
        elif self.turn < 4 and not party[0].has_died():
            self.user_choice(screen, event, party, enemies, items)
        else:
            self.enemy_attack(enemies, party)

        self.check_game(party, enemies)

    def start_battle(self, screen, event, party, items, enemies):
        if not self.battle_over:
            self.display_menu(screen, event, party, items, enemies)
            if self.turn == 1:
                party[0] = party[1]
            elif self.turn == 2:
                party[0] = party[2]
            elif self.turn == 3:
                party[0] = party[3]
            elif self.turn == 4:
                enemies[0] = enemies[1]
            elif self.turn == 5:
                enemies[0] = enemies[2]
            elif self.turn == 6:
                enemies[0] = enemies[3]
            else:
                self.turn = 1

        self.check_health(party, enemies)

        self.show_line(screen, party, enemies, self.turn)

    def attack(self, player, enemy):
        damage = player.get_strength()
        enemy.remove_current_hp(damage)
        self.turn += 1

    def magic_attack(self, player, enemy):
        magic = player.get_magic()
        player.remove_current_mp(1)
        enemy.remove_current_hp(magic)
        self.turn += 1

    def show_line(self, screen, party, enemies, attacker_index):
        character_y = 100
        for i in range(1, len(party)):
            if i == attacker_index:
                party[i].display_attack_sprite(screen, 900, character_y)
                character_y += 125
            else:
                party[i].display_idle_sprite(screen, 900, character_y)
                character_y += 125
        character_y = 100
        for i in range(1, len(enemies)):
            if i+3 == attacker_index:
                enemies[i].display_attack_sprite(screen, 300, character_y)
                character_y += 125
            else:
                enemies[i].display_idle_sprite(screen, 300, character_y)
                character_y += 125

    def enemy_attack(self, enemies, party):
        attack_index = random.randint(0,2)
        pygame.time.wait(2000)
        damage = enemies[self.turn-3].get_strength()
        party[attack_index].remove_current_hp(damage)
        self.turn += 1

    def user_choice(self, screen, event, party, enemies, items):
        if self.attack_button.get_clicked(event):
            self.attack_active = not self.attack_active
            self.spells_active = False
            self.items_active = False

        if self.magic_button.get_clicked(event):
            self.attack_active = False
            self.spells_active = not self.spells_active
            self.items_active = False
        if self.item_button.get_clicked(event):
            self.attack_active = False
            self.spells_active = False
            self.items_active = not self.items_active

        self.check_spells(party, enemies, event)

        if self.attack_active and not self.attack_button.get_hover():
            if enemies[1].get_hover():
                self.tooltip = "Select Enemy 1?"
                if enemies[1].get_clicked(event):
                    self.attack(party[0], enemies[1])
                    self.attack_active = False
            elif enemies[2].get_hover():
                self.tooltip = "Select Enemy 2?"
                if enemies[2].get_clicked(event):
                    self.attack(party[0], enemies[2])
                    self.attack_active = False
            elif enemies[3].get_hover():
                self.tooltip = "Select Enemy 3?"
                if enemies[3].get_clicked(event):
                    self.attack(party[0], enemies[3])
                    self.attack_active = False
            else:
                self.tooltip = "Choose a target"
        elif self.spells_active:
            party[0].display_spells(screen)
        elif self.items_active:
            items.display(screen, event, party, system.ITEM_X, system.ITEM_Y)

        if not self.attack_active:
            if enemies[1].get_hover():
                self.tooltip = enemies[1].get_name() + " HP: " + str(enemies[1].get_current_hp())
            elif enemies[2].get_hover():
                self.tooltip = enemies[2].get_name() + " HP: " + str(enemies[2].get_current_hp())
            elif enemies[3].get_hover():
                self.tooltip = enemies[3].get_name() + " HP: " + str(enemies[3].get_current_hp())
            else:
                self.tooltip = ""

        tooltip = system.MEDIUM_FONT.render(self.tooltip, 0, system.BLACK)
        screen.blit(tooltip, (system.TOOLTIP_X, system.TOOLTIP_Y))

    def check_spells(self, party, enemies, event):
        if self.spells_active and not self.magic_button.get_hover():
            if enemies[1].get_hover():
                self.tooltip = "Select Enemy 1?"
                if enemies[1].get_clicked(event):
                    self.magic_attack(party[0], enemies[1])
                    self.spells_active = False
            elif enemies[2].get_hover():
                self.tooltip = "Select Enemy 2?"
                if enemies[2].get_clicked(event):
                    self.magic_attack(party[0], enemies[2])
                    self.spells_active = False
            elif enemies[3].get_hover():
                self.tooltip = "Select Enemy 3?"
                if enemies[3].get_clicked(event):
                    self.magic_attack(party[0], enemies[3])
                    self.spells_active = False
            else:
                self.tooltip = "Choose a target"
        if not self.spells_active:
            if enemies[1].get_hover():
                self.tooltip = enemies[1].get_name() + " HP: " + str(enemies[1].get_current_hp())
            elif enemies[2].get_hover():
                self.tooltip = enemies[2].get_name() + " HP: " + str(enemies[2].get_current_hp())
            elif enemies[3].get_hover():
                self.tooltip = enemies[3].get_name() + " HP: " + str(enemies[3].get_current_hp())
            else:
                self.tooltip = ""

    def check_health(self, party, enemies):
        for x in range(0, len(party)):
            party[x].kill()
        for y in range(0, len(enemies)):
            enemies[y].kill()

    def check_game(self, party, enemies):
        if(party[1].has_died() and party[2].has_died() and party[3].has_died()):
            return True
        if(enemies[1].has_died() and enemies[2].has_died() and enemies[3].has_died()):
            return True
        return False