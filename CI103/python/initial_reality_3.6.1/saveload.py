import system


# Used to save game data to a text file
# Writes all stats and inventory items to a file
def save(characterArray, itemArray, cubeArray, level, saveFile):
    file = open(saveFile, "w")
    for x in range(0, 6):
        file.write(str(characterArray[x].get_max_hp())+"\n")
        file.write(str(characterArray[x].get_max_mp())+"\n")
        file.write(str(characterArray[x].get_current_hp())+"\n")
        file.write(str(characterArray[x].get_current_mp())+"\n")
        file.write(str(characterArray[x].get_strength())+"\n")
        file.write(str(characterArray[x].get_defense())+"\n")
        file.write(str(characterArray[x].get_magic())+"\n")
        file.write(str(characterArray[x].get_resistance())+"\n")
        file.write(str(characterArray[x].get_agility())+"\n")
        file.write(str(characterArray[x].get_evasion())+"\n")
        file.write(str(characterArray[x].get_accuracy())+"\n")
        file.write(str(characterArray[x].get_luck())+"\n")
        file.write(str(characterArray[x].get_max_hp())+"\n")
    file.write(str(itemArray.get_potions())+"\n")
    file.write(str(itemArray.get_elixirs())+"\n")
    file.write(str(itemArray.get_revives())+"\n")
    file.write(str(cubeArray.get_cube(system.POWER))+"\n")
    file.write(str(cubeArray.get_cube(system.MAGIC))+"\n")
    file.write(str(cubeArray.get_cube(system.MOVEMENT))+"\n")
    file.write(str(cubeArray.get_cube(system.FORTUNE))+"\n")
    for x in range(0, len(level.grid.nodes)):
        for y in range(0, len(level.grid.nodes[0])):
            file.write(str(level.grid.nodes[x][y].get_status())+"\n")
    file.close()


# Used to load save data from text file
# Sets all party stats to values saved in text file
def load(characterArray, itemArray, cubeArray, level, saveFile):
    file = open(saveFile, "r")
    for x in range(0, len(characterArray)):
        characterArray[x].set_max_hp(int(file.readline()))
        characterArray[x].set_max_mp(int(file.readline()))
        characterArray[x].set_current_hp(int(file.readline()))
        characterArray[x].set_current_mp(int(file.readline()))
        characterArray[x].set_strength(int(file.readline()))
        characterArray[x].set_defense(int(file.readline()))
        characterArray[x].set_magic(int(file.readline()))
        characterArray[x].set_resistance(int(file.readline()))
        characterArray[x].set_agility(int(file.readline()))
        characterArray[x].set_evasion(int(file.readline()))
        characterArray[x].set_accuracy(int(file.readline()))
        characterArray[x].set_luck(int(file.readline()))
        characterArray[x].set_max_hp(int(file.readline()))
    itemArray.set_potions(int(file.readline()))
    itemArray.set_elixirs(int(file.readline()))
    itemArray.set_revives(int(file.readline()))
    cubeArray.set_cube(system.POWER, int(file.readline()))
    cubeArray.set_cube(system.MAGIC, int(file.readline()))
    cubeArray.set_cube(system.MOVEMENT, int(file.readline()))
    cubeArray.set_cube(system.FORTUNE, int(file.readline()))
    for x in range(0, len(level.grid.nodes)):
        for y in range(0, len(level.grid.nodes[0])):
            level.grid.nodes[x][y].set_status(int(file.readline()))
    file.close()
