import pygame


# Used to swap screens
class ScreenSelect:
    def __init__(self):
        self.title = True
        self.shop = False
        self.grid = False
        self.battle = False

    def launch_title(self):
        self.title = True
        self.shop = False
        self.grid = False
        self.battle = False
        pygame.mixer.music.stop()
        pygame.time.wait(500)
        pygame.mixer.music.load("./resources/music/title.mp3")
        pygame.mixer.music.play()

    def launch_shop(self):
        self.title = False
        self.shop = True
        self.grid = False
        self.battle = False

    def launch_grid(self):
        self.title = False
        self.shop = False
        self.grid = True
        self.battle = False

    def launch_battle(self):
        self.title = False
        self.shop = False
        self.grid = False
        self.battle = True
        pygame.mixer.music.stop()
        pygame.time.wait(500)
        pygame.mixer.music.load("./resources/music/battle.mp3")
        pygame.mixer.music.play()

    def get_title(self):
        return self.title

    def get_shop(self):
        return self.shop

    def get_grid(self):
        return self.grid

    def get_battle(self):
        return self.battle
