import pygame
pygame.font.init()

# Global Constants

# Window Dimensions
WINDOW_WIDTH = 1280
WINDOW_HEIGHT = 720
WINDOW_CENTER_X = WINDOW_WIDTH / 2
WINDOW_CENTER_Y = WINDOW_HEIGHT / 2
FPS = 60

# Colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (200, 0, 0)

# Fonts
SMALL_FONT = pygame.font.Font("./resources/fonts/pixel.ttf", 18)
MEDIUM_FONT = pygame.font.Font("./resources/fonts/pixel.ttf", 30)
LARGE_FONT = pygame.font.Font("./resources/fonts/pixel.ttf", 50)

# main.py
BACK_BUTTON_X = 10
BACK_BUTTON_Y = 10

# button.py
# Used to manipulate position of selector that appears when hovering over a button
TOOLTIP_X = 255
TOOLTIP_Y = 10
SELECTOR_SPACING = 30

# title.py
# Used to manipulate main menu items
BUTTON_SPACING = 75

# battle.py
# Used to manipulate position of spells on battle screen
BUTTON_X = 80
BUTTON_Y = 560
BUTTON_SPACING_Y = 50
NAME_X = 1080
NAME_Y = 550
NAME_SPACING_Y = 50
ITEM_X = 330
ITEM_Y = 560
ITEM_SPACING_Y = 50
SPRITE_WIDTH = 75
SPRITE_HEIGHT = 150

# character.py
# Used to manipulate position of character stats on battle screen
STAT_SPACING_X = 10
STAT_SPACING_Y = 20
SPELL_INITIAL_X = 330
SPELL_INITIAL_Y = 560
SPELL_SPACING_X = 150
SPELL_SPACING_Y = 50
SPELL_MAX_X = SPELL_INITIAL_X + (3 * SPELL_SPACING_X)
SPELL_MAX_Y = SPELL_INITIAL_Y + (3 * SPELL_SPACING_Y)

# grid.py
# Used to manipulate position of grid nodes and stats
EMPTY_IMG = "./resources/images/grid/nodes/empty.png"
PREVIEW_IMG = "./resources/images/grid/nodes/preview.png"
AVAILABLE_IMG = "./resources/images/grid/nodes/available.png"
HP_IMG = "./resources/images/grid/nodes/hp.png"
MP_IMG = "./resources/images/grid/nodes/mp.png"
STRENGTH_IMG = "./resources/images/grid/nodes/strength.png"
DEFENSE_IMG = "./resources/images/grid/nodes/defense.png"
MAGIC_IMG = "./resources/images/grid/nodes/magic.png"
RESISTANCE_IMG = "./resources/images/grid/nodes/resistance.png"
AGILITY_IMG = "./resources/images/grid/nodes/agility.png"
ACCURACY_IMG = "./resources/images/grid/nodes/accuracy.png"
EVASION_IMG = "./resources/images/grid/nodes/evasion.png"
LUCK_IMG = "./resources/images/grid/nodes/luck.png"

EMPTY = 0
PREVIEW = 1
AVAILABLE = 2
ACTIVE = 3

POWER = 0
MAGIC = 1
MOVEMENT = 2
FORTUNE = 3

ROWS = 5
COLUMNS = 10
NODE_SIZE = 80
GRID_X = WINDOW_CENTER_X - (COLUMNS / 2.0) * NODE_SIZE
GRID_Y = 50
GRID_BOTTOM = GRID_Y + (ROWS * NODE_SIZE)
GRID_STAT_SPACING_X = 300
GRID_STAT_SPACING_X_2 = 250
GRID_STAT_SPACING_Y = 40
STAT_GRID_OFFSET_X = 40
STAT_GRID_OFFSET_Y = 20

# shop.py
# Used to mainpulate position of shop buttons
SHOP_SELLING_ITEM_X = 280
SHOP_ITEM_Y = 200
SHOP_INVENTORY_ITEM_X = 700
SHOP_ITEM_SPACING_Y = 50
SHOP_SELLING_Y = 100
SHOP_BUYING_TEXT_X = 680

# character.py
# Used to display death
DEATH_SPRITE = "./resources/images/battle/rip.png"