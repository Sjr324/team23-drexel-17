import pygame
import system
import button


# Class to display the shop screen
class Shop:
    def __init__(self):
        super().__init__()
        self.background = pygame.image.load("./resources/images/shop/background.png").convert()
        self.info_box = pygame.image.load("./resources/images/shop/info_box.png").convert()
        self.shop_box = pygame.image.load("./resources/images/shop/shop_box.png").convert()
        self.selling_text = system.MEDIUM_FONT.render("For Sale:", 0, system.BLACK)
        self.buying_text = system.MEDIUM_FONT.render("Your Inventory:", 0, system.BLACK)

    def run(self, screen, event, items, cubes):
        pygame.display.set_caption("Shop")
        screen.blit(self.background, (0, 0))
        screen.blit(self.info_box, (system.GRID_X, 0))
        screen.blit(self.shop_box, (system.GRID_X, system.GRID_Y))
        screen.blit(self.selling_text, (system.SHOP_SELLING_ITEM_X, system.SHOP_SELLING_Y))
        screen.blit(self.buying_text, (system.SHOP_BUYING_TEXT_X, system.SHOP_SELLING_Y))
        
        potions_sell = button.Button("Potion", "Restores 100 HP (Cost: 1 Power Cube)", system.BLACK)
        potions_sell.display_left_aligned(screen, system.SHOP_SELLING_ITEM_X, system.SHOP_ITEM_Y)
        elixirs_sell = button.Button("Elixir", "Restores 10 MP (Cost: 1 Magic Cube)", system.BLACK)
        elixirs_sell.display_left_aligned(screen, system.SHOP_SELLING_ITEM_X, system.SHOP_ITEM_Y + system.SHOP_ITEM_SPACING_Y)
        revives_sell = button.Button("Revive", "Reanimates character (Cost: 1 Fortune Cube)", system.BLACK)
        revives_sell.display_left_aligned(screen, system.SHOP_SELLING_ITEM_X, system.SHOP_ITEM_Y + (2 * system.SHOP_ITEM_SPACING_Y))
        
        potions_inventory = button.Button("(x " + str(items.get_potions()) + ") " + "Potion", "Restores 100 HP", system.BLACK)
        potions_inventory.display_left_aligned(screen, system.SHOP_BUYING_TEXT_X, system.SHOP_ITEM_Y)
        elixirs_inventory = button.Button("(x " + str(items.get_elixirs()) + ") " + "Elixir", "Restores 10 MP", system.BLACK)
        elixirs_inventory.display_left_aligned(screen, system.SHOP_BUYING_TEXT_X, system.SHOP_ITEM_Y + system.SHOP_ITEM_SPACING_Y)
        revives_inventory = button.Button("(x " + str(items.get_revives()) + ") " + "Revive", "Reanimates character", system.BLACK)
        revives_inventory.display_left_aligned(screen, system.SHOP_BUYING_TEXT_X, system.SHOP_ITEM_Y + (2 * system.SHOP_ITEM_SPACING_Y))

        power = button.Button("(x " + str(cubes.get_cube(system.POWER)) + ") " + "Power Cube", "Can be redeemed for stat upgrades", system.BLACK)
        power.display_left_aligned(screen, system.SHOP_BUYING_TEXT_X, system.SHOP_ITEM_Y + (3 * system.SHOP_ITEM_SPACING_Y))
        magic = button.Button("(x " + str(cubes.get_cube(system.MAGIC)) + ") " + "Magic Cube", "Can be redeemed for stat upgrades", system.BLACK)
        magic.display_left_aligned(screen, system.SHOP_BUYING_TEXT_X, system.SHOP_ITEM_Y + (4 * system.SHOP_ITEM_SPACING_Y))
        movement = button.Button("(x " + str(cubes.get_cube(system.MOVEMENT)) + ") " + "Movement Cube", "Can be redeemed for stat upgrades", system.BLACK)
        movement.display_left_aligned(screen, system.SHOP_BUYING_TEXT_X, system.SHOP_ITEM_Y + (5 * system.SHOP_ITEM_SPACING_Y))
        fortune = button.Button("(x " + str(cubes.get_cube(system.FORTUNE)) + ") " + "Fortune Cube", "Can be redeemed for stat upgrades", system.BLACK)
        fortune.display_left_aligned(screen, system.SHOP_BUYING_TEXT_X, system.SHOP_ITEM_Y + (6 * system.SHOP_ITEM_SPACING_Y))
        
        if potions_sell.get_clicked(event) and cubes.get_cube(system.POWER) >= 1:
            items.add_potions(1)
            cubes.remove_cube(system.POWER, 1)
        elif elixirs_sell.get_clicked(event) and cubes.get_cube(system.MAGIC) >= 1:
            items.add_elixirs(1)
            cubes.remove_cube(system.MAGIC, 1)
        elif revives_sell.get_clicked(event) and cubes.get_cube(system.FORTUNE) >= 1:
            items.add_revives(1)
            cubes.remove_cube(system.FORTUNE, 1)
