import pygame
import system
import screens
import title
import battle
import level
import shop
import character
import inventory
import button
import saveload


def main():
    # Initializes PyGame, window size, and a game clock
    pygame.init()
    screen = pygame.display.set_mode([system.WINDOW_WIDTH, system.WINDOW_HEIGHT])
    pygame.display.set_caption("Initial Reality")
    clock = pygame.time.Clock()
    done = False

    # Party initialization
    # Creates character objects which stores name, HP, MP, ATK, DEF, MAG, RES, AGI, EVA, ACC, LCK, and two sprites
    character1 = character.Character("Char1", 100, 10, 1, 2, 3, 4, 5, 6, 7, 8,
                                     "./resources/images/battle/characters/fire_mage_idle.png",
                                     "./resources/images/battle/characters/fire_mage_attack.png")
    character2 = character.Character("Char2", 200, 20, 21, 22, 23, 24, 25, 26, 27, 28,
                                     "./resources/images/battle/characters/witch_idle.png",
                                     "./resources/images/battle/characters/witch_attack.png")
    character3 = character.Character("Char3", 300, 30, 31, 32, 33, 34, 35, 36, 37, 38,
                                     "./resources/images/battle/characters/water_mage_idle.png",
                                     "./resources/images/battle/characters/water_mage_attack.png")

    # Creates the inventory objects
    items = inventory.Items()
    cubes = inventory.Cubes()

    # Creates a party list
    party = [character1, character1, character2, character3]

    # Appends spells to characters
    character1.add_spell("FLAME", "Casts a fire spell, effective vs ice")
    character2.add_spell("FREEZE", "Casts an ice spell, effective vs fire")
    character2.add_spell("BOLT", "Casts a lightning spell, effective vs water")
    character3.add_spell("WAVE", "Casts a water spell, effective vs lightning")

    # Initializes enemies with same stats as characters
    enemy1 = character.Character("Enemy1", 100, 40, 41, 42, 43, 44, 45, 46, 47, 48,
                                 "./resources/images/battle/characters/zombie_idle.png",
                                 "./resources/images/battle/characters/zombie_attack.png")
    enemy2 = character.Character("Enemy2", 100, 40, 41, 42, 43, 44, 45, 46, 47, 48,
                                 "./resources/images/battle/characters/genie_idle.png",
                                 "./resources/images/battle/characters/genie_attack.png")
    enemy3 = character.Character("Enemy3", 100, 40, 41, 42, 43, 44, 45, 46, 47, 48,
                                 "./resources/images/battle/characters/zombie_idle.png",
                                 "./resources/images/battle/characters/zombie_attack.png")

    # Creates an enemy list
    enemies = [enemy1, enemy1, enemy2, enemy3]

    # Screens and navigation classes
    title_screen = title.Title()
    battle_screen = battle.Battle()
    level_screen = level.Level()
    shop_screen = shop.Shop()

    # Screen swapping classes
    current_screen = screens.ScreenSelect()

    # Buttons
    back_button = button.Button("< BACK", "Return to title screen", system.BLACK)
    save_button = button.Button("Save", "", system.BLACK)
    load_button = button.Button("Load", "", system.BLACK)

    # Initialize game loop
    while not done:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                done = True
            # Testing purposes only
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_1:
                    cubes.add_cube(system.POWER, 1)
                elif event.key == pygame.K_2:
                    cubes.add_cube(system.MAGIC, 1)
                elif event.key == pygame.K_3:
                    cubes.add_cube(system.MOVEMENT, 1)
                elif event.key == pygame.K_4:
                    cubes.add_cube(system.FORTUNE, 1)
                elif event.key == pygame.K_5:
                    items.add_potions(1)
                elif event.key == pygame.K_6:
                    items.add_elixirs(1)
                elif event.key == pygame.K_7:
                    items.add_revives(1)
                elif event.key == pygame.K_8:
                    party[0].add_current_hp(-8)
                elif event.key == pygame.K_9:
                    party[0].add_current_mp(-4)

            # Screens
            if current_screen.get_title():
                title_screen.run(screen, event, current_screen)
            elif current_screen.get_battle():
                battle_screen.start_battle(screen, event, party, items, enemies)
            elif current_screen.get_grid():
                level_screen.display(screen, event, party, cubes)
            elif current_screen.get_shop():
                shop_screen.run(screen, event, items, cubes)

            # If any screen but the title screen is loaded, display the back button
            if not current_screen.get_title():
                back_button.display_left_aligned(screen, system.BACK_BUTTON_X, system.BACK_BUTTON_Y)
                # If the back button is clicked, load the title screen
                if back_button.get_clicked(event):
                    current_screen.launch_title()

            # Creates a list of all available characters, party, and enemies included
            characArr = [character1, character2, character3, enemy1, enemy2, enemy3]

            # If the title screen is loaded, display the save and load buttons
            if current_screen.get_title():
                save_button.display_centered(screen, system.WINDOW_CENTER_Y + (3 * system.BUTTON_SPACING))
                if save_button.get_clicked(event):
                    saveload.save(characArr, items, cubes, level_screen, "save")
            if current_screen.get_title():
                load_button.display_centered(screen, system.WINDOW_CENTER_Y + (4 * system.BUTTON_SPACING))
                if load_button.get_clicked(event):
                    saveload.load(characArr, items, cubes, level_screen, "save")

        # Update screen at the designated FPS
        pygame.display.update()
        clock.tick(system.FPS)
    pygame.quit()

if __name__ == "__main__":
    main()
