import pygame
import system


# Class used to create clickable buttons
class Button(pygame.sprite.Sprite):
    def __init__(self, text, tooltip, color):
        super().__init__()
        self.text = system.MEDIUM_FONT.render(text, 0, color)
        self.rect = self.text.get_rect()
        self.selector = system.MEDIUM_FONT.render(">", 0, color)
        self.selector_rect = self.selector.get_rect()
        self.tooltip = system.MEDIUM_FONT.render(tooltip, 0, system.BLACK)
        self.tooltip_rect = self.tooltip.get_rect()

    # Will display a button left aligned (battle screen)
    def display_left_aligned(self, screen, x, y):
        self.rect.left = x
        self.rect.y = y
        screen.blit(self.text, self.rect)
        self.display_selector(screen)
        self.display_tooltip(screen)

    # WIll display a button centered (title screen)
    def display_centered(self, screen, y):
        self.rect.centerx = system.WINDOW_CENTER_X
        self.rect.y = y
        screen.blit(self.text, self.rect)
        self.display_selector(screen)

    # Will display a button's tooltip
    def display_tooltip(self, screen):
        if self.rect.collidepoint(pygame.mouse.get_pos()):
            self.tooltip_rect.x = system.TOOLTIP_X
            self.tooltip_rect.y = system.TOOLTIP_Y
            screen.blit(self.tooltip, self.tooltip_rect)

    # Displays an arrow next to the text on mouse hover
    def display_selector(self, screen):
        if self.rect.collidepoint(pygame.mouse.get_pos()):
            self.selector_rect.x = self.rect.x - system.SELECTOR_SPACING
            self.selector_rect.y = self.rect.y
            screen.blit(self.selector, self.selector_rect)

    # Returns true if the button is clicked
    def get_clicked(self, event):
        if self.rect.collidepoint(pygame.mouse.get_pos()):
            if event.type == pygame.MOUSEBUTTONDOWN:
                return True

    # Returns true if the button is hovered over
    def get_hover(self):
        if self.rect.collidepoint(pygame.mouse.get_pos()):
            return True